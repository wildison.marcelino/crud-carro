import {Component, OnInit, ViewChild} from '@angular/core';
import {Icarro} from "../models/carro";
import {CarroService} from "../services/carro.service";
import {DialogCarroComponent} from "./dialog-carro/dialog-carro.component";
import {MatDialog} from "@angular/material/dialog";
import {MatTable} from "@angular/material/table";
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {DialogDeletarComponent} from "./dialog-deletar/dialog-deletar.component";
import {DialogDepreciarComponent} from "./dialog-depreciar/dialog-depreciar.component";
import {DialogDepreciarTodosComponent} from "./dialog-depreciar-todos/dialog-depreciar-todos.component";


@Component({
  selector: 'app-carro',
  templateUrl: './carro.component.html',
  styleUrls: ['./carro.component.css']
})
export class CarroComponent implements OnInit {
  carro: Icarro = {
    modelo: '',
    marca: '',
    cor: '',
    ano: null,
    valor: null,
  };

  displayedColumns: string[];
  dataSource: MatTableDataSource<Icarro>;

  carros: Icarro[];
  showCarro: Icarro;

  @ViewChild('table') table!: MatTable<Icarro>;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private carroService: CarroService,
              private dialog: MatDialog,
  ) {
  }

  ngOnInit(): void {
    this.displayedColumns = ['id', 'modelo', 'marca', 'cor', 'ano', 'valor', 'action'];
    this.lerCarros(this.carro);
  }

  lerCarros(carro: Icarro) {
    this.carroService.read().subscribe(carros => {
      if (carros != null && carros.length > 0) {
        this.showCarro = carros[0];
        this.preencherTabela(carros);
      }
    })
  }

  depreciar(id: number) {
    this.carroService.readById(id).subscribe(carro => {
      if (carro.valor <= 10000) {
        this.carroService.showMessage('Valor abaixo da media para depreciação!')
        this.lerCarros(carro)
      } else {
        carro.valor = carro.valor - carro.valor * 0.02
        this.carroService.update(carro).subscribe(() => {
          this.carroService.showMessage('Valor Depreciado com Sucesso')
          this.lerCarros(carro)
        })
      }
    })
  }

  depreciarTodos(carro: Icarro[]) {
    this.carroService.read().subscribe(carros => {
      if (carros != null) {
        for (let carro of carros) {
          this.depreciar(carro.id)
          this.lerCarros(this.carro)
        }
      }
    })
  }

  private preencherTabela(result: Icarro[]) {
    this.dataSource = new MatTableDataSource(result);
  }

  ConfirmaExclusao(id: number) {
    let dialogRef = this.dialog.open(DialogDeletarComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'confirm') {
        this.carro.id = id
        this.deletarCarro(id)
      }
    })
  }

  ConfirmaDepreciacao(id: number) {
    let dialogRef = this.dialog.open(DialogDepreciarComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'confirm') {
        this.carro.id = id
        this.depreciar(id)
      }
    })
  }

  ConfirmaDepreciacaoTodos(carro: Icarro) {
    let dialogRef = this.dialog.open(DialogDepreciarTodosComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'confirm') {
        this.depreciarTodos(this.carros)
      }
    })
  }

  deletarCarro(idD: number): void {
    this.carroService.delete(idD).subscribe(() => {
      this.carroService.showMessage('Carro deletado!')
      this.lerCarros(this.carro);
    })
  }

  mostrarCarro(id: number) {
    this.carroService.readById(id).subscribe(result => {
      this.showCarro = result;
    });
  }

  abrirDialogCarro(carro: Icarro): void {
    const dialogRef = this.dialog.open(
      DialogCarroComponent,
      {
        width: '280px',
        height: '500px',
        data: carro
      }
    );
    dialogRef.afterClosed().subscribe(() => this.lerCarros(carro));
  }
}
