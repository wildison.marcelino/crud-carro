import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Icarro} from "../../models/carro";
import {CarroService} from "../../services/carro.service";
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-dialog-carro',
  templateUrl: './dialog-carro.component.html',
  styleUrls: ['./dialog-carro.component.css'],
})
export class DialogCarroComponent implements OnInit {

  formGroup: FormGroup;

  constructor(public dialogRef: MatDialogRef<DialogCarroComponent>,
              private carroService: CarroService,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder,
              @Inject(MAT_DIALOG_DATA) public data: Icarro,
  ) {
    this.buildFormGroup();
    if (data) {
      this.formGroup.patchValue(data);
    }
  }

  ngOnInit(): void {
  }

  private buildFormGroup(): void {
    this.formGroup = this.formBuilder.group({
      id: [null],
      modelo: [null, Validators.required],
      marca: [null, Validators.required],
      cor: [null, [Validators.required, Validators.minLength(3)]],
      ano: [null, [Validators.required, Validators.maxLength(4), Validators.minLength(4), Validators.min(2000)]],
      valor: [null, [Validators.required, Validators.minLength(5),  Validators.min(5000)]],
    })
  }

  onSubmit() {
    const carroCompleto: Icarro = this.formGroup.value;
    if (carroCompleto.id != null) {
      this.carroService.update(carroCompleto).subscribe(() => {
        this.carroService.showMessage('Carro Atualizado!');
        this.dialogRef.close();
      })
    } else {
      this.carroService.criar(carroCompleto).subscribe(() => {
        this.carroService.showMessage("Carro Cadastrado com Sucesso!");
        this.dialogRef.close();
      })
    }
  }


  close(): void {
    this.dialogRef.close();
  }
}
