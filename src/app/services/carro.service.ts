import {Injectable} from '@angular/core';
import {MatSnackBar} from "@angular/material/snack-bar";
import {HttpClient} from "@angular/common/http";
import {Icarro} from "../models/carro";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CarroService {

  baseUrl = "http://localhost:8000/carros"

  constructor(private snackBar: MatSnackBar,
              private http: HttpClient) {
  }

  showMessage(msg: string): void {
    this.snackBar.open(msg, 'x', {
      duration: 3000,
      horizontalPosition: "right",
      verticalPosition: "top",

    })
  }

  criar(carro: Icarro): Observable<Icarro> {
    return this.http.post<Icarro>(this.baseUrl, carro)
  }

  read(): Observable<Icarro[]> {
    return this.http.get<Icarro[]>(this.baseUrl)
  }

  readById(id: number): Observable<Icarro> {
    const url = `${this.baseUrl}/${id}`
    return this.http.get<Icarro>(url)
  }

  update(carro: Icarro): Observable<Icarro> {
    const url = `${this.baseUrl}/${carro.id}`
    return this.http.put<Icarro>(url, carro)
  }

  delete(idD: number): Observable<Icarro> {
    const url = `${this.baseUrl}/${idD}`;
    return this.http.delete<Icarro>(url);
  }

}
