export interface Icarro {
  id?: number,
  modelo: string,
  marca: string,
  ano: number,
  cor: string
  valor: number,
}
